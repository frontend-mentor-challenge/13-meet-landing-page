# Frontend Mentor - Meet landing page solution

This is a solution to the [Meet landing page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/meet-landing-page-rbTDS6OUR). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover states for interactive elements

### Screenshot

![Mobile](./screenshots/mobile.gif)
![Tablet](./screenshots/tablet.gif)
![Desktop](./screenshots/desktop.gif)

### Links

- Solution URL: [Gitlab Repository](https://gitlab.com/frontend-mentor-challenge/13-meet-landing-page)
- Live Site URL: [Live](https://meet-landing-page.onrender.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [Normalize](https://necolas.github.io/normalize.css/) - CSS Reset
- [Google Fonts](https://fonts.google.com/)
- [SASS](https://sass-lang.com/) - SASS
- [Gulp](https://gulpjs.com/) - Gulp

### What I learned

I use this code to style number divider section

To see how you can add code snippets, see below:

```html
<div class="numbers">
  <div class="numbers__line"></div>
  <div class="numbers__number">01</div>
</div>
```
```scss
&__line {
    width: .1rem;
    height: 8.4rem;
    border: 1px solid v.$mandrake;
    opacity: .25;
}

&__number {
    display: grid;
    place-items: center;
    background-color: v.$dr-white;
    width: 5.6rem;
    height: 5.6rem;
    border: 1px solid rgba(v.$mandrake, .25);
    border-radius: 50%;
    font-weight: 900;
}
```

### Continued development

### Useful resources

- [Figma](https://figma.com) - For design

## Author

- Website - [Issac Leyva](In construction)
- Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)

## Acknowledgments