const {src,  dest, watch, series} = require('gulp')
const sass = require('gulp-sass')(require('sass'))
const cssnano = require('gulp-cssnano')
const imagemin = require('gulp-imagemin')
const terser = require('gulp-terser')

// Contains a paths to files
const paths = {
    scss: "src/scss/**/*.scss",
    js: "src/js/**/*.js",
    img: "src/images/**/*"
}

/* Transpile SASS to CSS files */
function css() {
    return (
        src(paths.scss)
        .pipe(sass({
            includePaths: ['node_modules']
        }))
        .pipe(cssnano())
        .pipe(dest("build/css"))
    )
}

/* Transpile Javascript files */
function javascript() {
    return (
        src(paths.js)
        .pipe(terser())
        .pipe(dest("build/js"))
    )
}

/* Optimize images */
function images() {
    return (
        src(paths.img)
        .pipe(imagemin())
        .pipe(dest("build/img"))
    )
}

/* Observe file's changes */
function watchFiles() {
    watch(paths.scss, css)
    watch(paths.js, javascript)
}


exports.css = css
exports.js = javascript
exports.images = images
exports.watchFiles = watchFiles
exports.dev =series(css, javascript, images)